## Available Scripts

In the project directory, you can run:

### Build docker image
```sh
docker-compose build
```

### Run docker container using existing image
```sh
docker-compose up -d
```

### List all ruuning docker processes
```sh
docker ps
```

### Log & debug docker processes
```sh
docker logs -f processId
```

### Hurray!! app is now ready for development
For frontend application: [http://localhost:3005](http://localhost:3005)

For backend application: [http://localhost:3006/api/fruits](http://localhost:3006/api/fruits)

### Other usefull scripts to setup docker in your project

**Create a [Dockerfile](./Dockerfile) in the project root.** *Make sure you have* [Docker](https://www.docker.com/products/docker-desktop) & [docker-compose](https://docs.docker.com/compose/install/) *installed in your system globally.*

**Build** image of the application using `-t tag-name` flag
```bash
docker build -t tag-name .
```

**List** docker images
```bash
docker images

// or

docker image ls
```

**Run** a docker image
```bash
docker run tag-name
```

**Run** a docker image in ditached mode or background or as a `Deamon process` using `-d` flag
```bash
docker run -d tag-name
```

**Run** a docker image with `port forwarding`. Here app running in port `3000` will be forwarded to port `80`
```bash
docker run -d -p 80:3000 tag-name
```

**List all** the processes/containers
```bash
docker ps -a
```

**Check** docker `logs` using `-f` flag. `-f` stands for follow
```bash
docker logs -f containerId
```

**Start/Stop** a container in interactive mode using `-i` flag
```bash
docker start -i containerId

// or

docker stop containerId
```

**Remove** stopped containers/images
```bash
docker container prune

// or

docker image prune
```

**Cleanup** all projects from docker through `terminal` using `-f` flag. `-f` stands for force
```sh
docker container rm -f $(docker container ls -aq)
docker image rm -f $(docker image ls -aq)

// or

docker container rm -f $(docker container ls -aq); docker image rm -f $(docker image ls -aq)
```

**Pull** the dokcer image from [Docker hub](https://hub.docker.com/search?q=&type=image)
```bash
docker pull ubuntu
```

**Run** a docker image in **interactive mode** using `-i` & `-t` flag. `-t` stands for `pseudo-TTY` 
```bash
docker run -i -t ubuntu

// or

docker run -it ubuntu
```

**List all** docker **networks**
```bash
docker network ls
```

Login into docker **container shell**
```bash
docker exec -it containerId sh
```

Docker network **checkup** when logged in into shell. Check available [services](./docker-compose.yml)
```bash
ping serviceName

// example

ping frontend
```
