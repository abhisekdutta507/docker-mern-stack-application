import React from 'react';
import './styles.scss';

export const Card = ({
  value = '',
  className = '',
  onDelete = () => {}
}) => {

  return (
    <div className={`card flex items-center ${className}`}>
      <div className="card-identifier full-height"></div>
      <div className="flex pad-md items-center justify-between full-width">
        <div className="font-color-light">{value}</div>
        <button title="action-button" className={`pad-reset inset-reset card-remove-btn`} onClick={onDelete}>
          <i className="fas fa-minus-circle fa-lg font-color-danger"></i>
        </button>
      </div>
    </div>
  );
};

