import './App.scss';
import React, { useEffect, useState } from 'react';
import { useFruits } from './hook';
import { Input, Card } from './component';
import { cloneDeep } from './util';

function App() {
  const [input, setInput] = useState('');
  const [{ isReady, data }] = useFruits([]);
  const [fruits, setFruits] = useState([]);

  /**
   * @description load the initial fruits
   */
  useEffect(() => {
    if (isReady && data.length) {
      setFruits(data);
    }
  }, [isReady]);

  const onInput = (e) => {
    e.preventDefault();

    if (input) {
      const nextFruits = cloneDeep([...fruits, input]);
      setFruits(nextFruits);
      setInput('');
    }
  }

  const onDelete = (index) => {
    const nextFruits = cloneDeep(fruits);
    nextFruits.splice(index, 1);
    setFruits(cloneDeep(nextFruits));
  }

  return (
    <div className="pad-xl">
      <p className="inset-top-reset inset-bottom-md font-color-light">Input</p>
      <form onSubmit={onInput}>
        <Input placeholder="Fruit" onChange={(e) => setInput(e.target.value)} value={input} />
      </form>
      {
        fruits.length
          ? <>
              <p className="inset-top-xl inset-bottom-md font-color-light">Fruit List</p>
              {
                fruits.map((item, key) => <Card key={key} value={item} className="inset-vert-md" onDelete={() => onDelete(key)} />)
              }
            </>
          : null
      }
    </div>
  );
}

export default App;
