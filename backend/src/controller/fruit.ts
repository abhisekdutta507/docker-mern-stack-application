import { Request, Response } from 'express'

export default class Fruit {
  private fruits: string[] = [
    'Apple',
    'Banana',
    'Mango',
    'Orange'
  ]

  constructor() {
    
  }

  find = async (req: Request, res: Response) => {
    return res.status(200).send({
      message: { type: 'success' },
      data: this.fruits
    })
  }

}