import Fruit from './fruit'

class Controller {

  public fruit: Fruit

  constructor() {
    this.fruit = new Fruit()
  }

}

export default new Controller()