import express from 'express'
import http from 'http'
import cors from 'cors'
import { config } from 'dotenv'
import { v1 } from './router'

/**
 * @description create an express application.
 */
const app = express()
/**
 * @description configure the .env file.
 */
config()
 
/**
 * @description cross-origin domain blocking.
 */
app.use(cors())

/**
 * @description parse the request body into the below options.
 */
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

/**
 * @description bind the version control routes with app.
 */
app.use('/api', v1())

/**
 * @description create a HTTP server.
 */
const server = new http.Server(app)

/**
 * @description sail the app on the server with a given PORT.
 */
server.listen(process.env.PORT, () => console.log(`app is running at http://localhost:${process.env.PORT}`))
