import { Router } from 'express'
import Controller from './controller'

/**
 * @description version 1 API routes
 */
export const v1 = () => {
  /**
   * @description connect the mongodb database
   */
  const router = Router()

  const { fruit } = Controller

  /**
   * @description fruit apis
   */
  router.get('/fruits', fruit.find)

  return router
}